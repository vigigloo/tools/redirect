ARG BASE_IMAGE=nginx:1.21.5-alpine
FROM $BASE_IMAGE

COPY ./start.sh /usr/local/bin/

RUN chmod +x /usr/local/bin/start.sh

EXPOSE 80

CMD ["start.sh"]
