variable "redirect_to" {
  type = string
}

variable "redirect_from" {
  type = string
}